#!/bin/bash

[ -d .cache/paperview ] || mkdir -p .cache/paperview
cd .cache/paperview
git clone https://github.com/glouw/paperview.git
cd paperview
make
cp ./paperview $HOME/bin/paperview
