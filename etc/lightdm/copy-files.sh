#!/bin/sh

rm /etc/lightdm/lightdm.conf
rm /etc/lightdm/lightdm-gtk-greeter.conf
cp ./etc/lightdm/lightdm.conf /etc/lightdm/
cp ./etc/lightdm/lightdm-gtk-greeter.conf /etc/lightdm/
