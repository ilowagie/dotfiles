#!/bin/sh

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
case "$HOSTNAME" in
    ("primus")      MONITOR=HDMI-A-1
                    CONFIG="$HOME/.config/polybar/primus_bspwm" ;;
    ("blauwedel")   MONITOR=$(polybar -m | tail -1 | sed -e 's/:.*$//g')
                    CONFIG="$HOME/.config/polybar/blauwedel_bspwm" ;;
esac

export MONITOR
polybar -c $CONFIG -r example &
