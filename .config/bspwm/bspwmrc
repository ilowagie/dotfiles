#! /bin/sh

###########
# Startup #
###########

source .cache/wal/colors.sh

sxhkd &
[ -z LDM_ACTIVE ] || light-locker --lock-after-screensaver=0 &
lxsession &

#picom -b --config $HOME/.config/picom/picom.conf

case "$HOSTNAME" in
    ("primus")      bspc monitor DisplayPort-1  -d 1 2 3 4 0
                    bspc monitor DisplayPort-0  -d 5 6 7 8 9
                    bspc config -m DisplayPort-1 top_padding 19 ;;
    ("blauwedel")   bspc monitor eDP1           -d 1 2 3 4 5 6 7 8 9 0
                    bspc config -m eDP1 top_padding 19 ;;
esac

bspc config ignore_ewmh_struts true

paperview $HOME/.config/background/moving/cliff 7 &

if [[ $HOSTNAME == primus ]]
then
    solaar --window=hide &
    # /usr/lib/notification-daemon-1.0/notification-daemon &
    # ^- don't really like this notification daemon
    /usr/bin/dunst &
else
    /usr/bin/dunst &
    nm-applet &
fi

udiskie -At &
$HOME/.config/polybar/launch_bspwm.sh &
paperview $HOME/.config/background/moving/cliff 7 &

bspc config border_width         2
bspc config window_gap           0

bspc config normal_border_color     $background
bspc config active_border_color     $color3
bspc config focused_border_color    $color1
bspc config presel_feedback_color   $color2

bspc config split_ratio             0.52
bspc config borderless_monocle      true
bspc config gapless_monocle         true
bspc config focus_follows_pointer   true
bspc config pointer_follows_focus   true
bspc config pointer_follows_monitor true

# Float Xephyr nested X server windows so size can be controlled
bspc rule -a Xephyr state=floating

bspc rule -a Steam state=floating
bspc rule -a Steam monitor=DisplayPort-1
bspc rule -a explorer.exe layer=above state=floating
bspc rule -a Qemu-system-x86_64 state=floating

bspc rule -a firefox:*:Picture-in-Picture sticky=on state=floating

# Floating desktop
bspwm_floating_desktop.sh &

# Scratchpad terminal
bspc rule -a scratchpad sticky=on state=floating hidden=on center=on rectangle=1000x300
termite --class=scratchpad &
# Scratchpad calculator
bspc rule -a scratchpad_calc sticky=on state=floating hidden=on center=on rectangle=800x400
gnome-calculator --class=scratchpad_calc --mode=advanced &
