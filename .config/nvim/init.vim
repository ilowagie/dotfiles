"  ______                               __
" /\__  _\          __                 /\ \                    __
" \/_/\ \/     ___ /\_\     __     ___ \ \/  ____      __  __ /\_\    ___ ___   _ __   ___
"    \ \ \   /' _ `\/\ \  /'_ `\  / __`\\/  /',__\    /\ \/\ \\/\ \ /' __` __`\/\`'__\/'___\
"     \_\ \__/\ \/\ \ \ \/\ \L\ \/\ \L\ \  /\__, `\   \ \ \_/ |\ \ \/\ \/\ \/\ \ \ \//\ \__/
"     /\_____\ \_\ \_\ \_\ \____ \ \____/  \/\____/    \ \___/  \ \_\ \_\ \_\ \_\ \_\\ \____\
"     \/_____/\/_/\/_/\/_/\/___L\ \/___/    \/___/      \/__/    \/_/\/_/\/_/\/_/\/_/ \/____/
"                           /\____/
"                           \_/__/
"
"   (init.vim voor de neovim boys)


syntax on

set number
set relativenumber
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set nohlsearch
set noerrorbells
set smartindent
set smartcase                   " no case sensitivity in searches untill capital letter is used
set noswapfile
set nobackup
set undodir=~/.cache/nvim/undo
set undofile
set incsearch                   " results come up while typing
set cmdheight=2
set cursorcolumn
set cursorline
set updatetime=50

call plug#begin('~/.config/nvim/plugged')

    " colorschemes
    Plug 'gruvbox-community/gruvbox'
    Plug 'cocopon/iceberg.vim'

    " Misc plugins
    " vim-polyglot still needed with treesitter?
    Plug 'sheerun/vim-polyglot'
    Plug 'jremmen/vim-ripgrep'
    Plug 'mbbill/undotree'
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'majutsushi/tagbar'
    Plug 'godlygeek/tabular'
    Plug 'chrisbra/NrrwRgn'
    Plug 'KKPMW/vim-sendtowindow'
    Plug 'guns/vim-sexp'
    Plug 'lambdalisue/suda.vim'
    Plug 'igankevich/mesonic'
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug'] }

    Plug 'kynan/dokuvimki', {'on': 'DokuVimKi', 'do': 'git checkout fix-idiotic-stuff && git merge master', 'branch': 'master'}

    " LSP stuff
    Plug 'neovim/nvim-lspconfig'
    Plug 'hrsh7th/nvim-cmp'
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
    Plug 'hrsh7th/cmp-buffer'
    Plug 'hrsh7th/cmp-path'
    Plug 'onsails/lspkind.nvim'
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
    Plug 'nvim-treesitter/playground',
    Plug 'dcampos/nvim-snippy'

    " And now some plugins of the almighty tpope:
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-abolish'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-sexp-mappings-for-regular-people'

    " Local Plugins
    Plug '~/.config/nvim/plugged/jump-pair'
    Plug '~/.config/nvim/plugged/quick-term'
    Plug '~/coding/glacier_nvim'

call plug#end()

"let g:gruvbox_contrast_dark='medium'
"let g:gruvbox_invert_selection='0'
set background=dark
" let g:nord_contrast = v:true
" let g:nord_disable_background = v:true
" let g:nord_italic = v:false
" colorscheme nord
"colorscheme iceberg
set termguicolors
"let g:glacier_debug = v:true
colorscheme glacier_nvim

" fix some highlighting as soon as colorscheme is loaded
"hi! ErrorMsg ctermfg=Black ctermfg=Red
"hi! Error ctermfg=Red ctermbg=Black
"hi! RedrawDebugRecompose ctermfg=Black
"hi! Warning ctermfg=Yellow ctermbg=black
"hi! WarningMsg ctermfg=Black ctermbg=Yellow
"hi! PmenuThumb ctermfg=Black
"hi! NvimInternalError ctermfg=Red ctermbg=Black

let mapleader=" "

let g:netrw_banner=0        " remove big banner from the file explorer
let g:netrw_liststyle=3     " tree view
" let g:netrw_browse_split=4  " opens file in big window rather than in the small size 25 window
let g:netrw_winsize=15

let g:fzf_tags_command='[ -f .ctagsignore ] && ctags -R --exclude=@.ctagsignore || ctags -R'
let g:sendtowindow_use_defaults=0

let g:sexp_enable_insert_mode_mappings=0

" dokuvimki
let g:DokuVimKi_URL = 'http://localhost:8000'
let g:DokuVimKi_USER = 'ilowagie'
let g:DokuVimKi_PASS = 'notarealpassword'
let g:DokuVimKi_HTTP_BASIC_AUTH = 1

let g:mkdp_auto_close = 0
let g:mkdp_auto_start = 0

nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

" open netrw in small split on the side
"nmap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 25<CR>
nmap <leader>pv :Lexplore<CR>

" undotree plugin
nmap <leader>u :UndotreeShow<CR>
" ripgrep
nmap <leader>rg :Rg<space>
" fzf plugins
nmap <leader>df :Files<CR>
nmap <silent> <leader>f :call fzf#run({ 'right': winwidth('.') / 2, 'sink': 'vertical botright split' })<CR>
nmap <silent> <leader>v :call fzf#run({ 'down': winheight('.') / 2, 'sink': 'botright split' })<CR>
nmap <leader>dt :Tags<CR>
nmap <leader>de :BTags<CR>
" sendtowindow plugin
nmap <leader>sl <Plug>SendRight
xmap <leader>sl <Plug>SendRightV
nmap <leader>sh <Plug>SendLeft
xmap <leader>sh <Plug>SendLeftV
nmap <leader>sk <Plug>SendUp
xmap <leader>sk <Plug>SendUpV
nmap <leader>sj <Plug>SendDown
xmap <leader>sj <Plug>SendDownV

" quickfix and location list stuff
"   quickfix list is a list of locations over your VIM instance
"   location list is a list which is local to a window
" up to 10 of each is kept
" one can go between lists w/
" :colder :cnewer
" :lolder :lnewer
" use :cdo to execute a command over each item

" quickfix list mappings
nmap <leader>qo :copen<CR>
nmap <leader>qx :cclose<CR>
nmap <leader>qn :cnext<CR>
nmap <leader>qp :cprev<CR>
nmap <leader>qf :cfirst<CR>
nmap <leader>ql :clast<CR>
nmap <leader>qr :colder<CR>
nmap <leader>qk :cnewer<CR>
" location list mappings
nmap <leader>co :lopen<CR>
nmap <leader>cx :lclose<CR>
nmap <leader>cn :lnext<CR>
nmap <leader>cp :lprev<CR>
nmap <leader>cf :lfirst<CR>
nmap <leader>cl :llast<CR>
nmap <leader>cr :lolder<CR>
nmap <leader>ck :lnewer<CR>

" fold function: works with most languages even if LSP/CoC/linter isn't working quite right
nnoremap <leader>ff $zf%

" remaps from https://www.youtube.com/watch?v=hSHATqh8svM
nnoremap Y y$
nnoremap J mzJ`z
"   undo breakpoints
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u
"   update jumplist if a jump is larger than 5 lines
nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'

" spellcheck
map <F6> :setlocal spell! spelllang=en_us<CR>
map <F8> :TagbarToggle<CR>

if executable('rg')
    let g:rg_derive_root='true'
endif

" Tips from the CIA (https://wikileaks.org/ciav7p1/cms/page_4849889.html)
" cmap w!! w !sudo tee % > /dev/null
" cmap bin %!xxd
" ^- both not in neovim. For w!! we have SudaWrite

" remove white spaces at line ends
autocmd BufWritePre * %s/\s\+$//e

" other highlighting: italics

hi Comment cterm=italic

" autocomplete behavior
set completeopt=menu,menuone,noselect
"inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"inoremap <expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
"inoremap <expr> <S-TAB> pumvisible() ? "\<C-p>" : "\<TAB>"
set shortmess+=c

" always load extraluacfg -> set loaded to false otherwise, no reload of lua config
lua package.loaded['extraluacfg'] = nil
lua require('extraluacfg')

" folding
" set foldmethod=expr
" set foldexpr=nvim_treesitter#foldexpr()
set foldlevel=99
