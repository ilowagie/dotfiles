-----------------
-- Colorscheme --
-----------------

-- require'glacier_nvim'.setup ({
--     override = {
--         background = false,
--         float_background = false,
--     }
-- })

------------------------------
-- Treesitter configuration --
------------------------------

local treesitter_langs = {
    'bash',
    'bibtex',
    'c',
    'cpp',
    'css',
    'haskell',
    'html',
    'latex',
    'lua',
    'javascript',
    'lua',
    'python',
    'rust',
    'verilog',
    'query',
    'sql',
    'meson',
    'twig',
    'scss',
}

require 'nvim-treesitter'.define_modules {
    folding_extra = {
        attach = function(bufnr, lang)
            vim.api.nvim_win_set_option(vim.fn.bufwinid(bufnr), 'foldmethod', 'expr')
            vim.api.nvim_win_set_option(vim.fn.bufwinid(bufnr), 'foldexpr', 'nvim_treesitter#foldexpr()')
        end,
        detach = function(bufnr, lang)
            vim.api.nvim_win_set_option(vim.fn.bufwinid(bufnr), 'foldmethod', 'manual')
            vim.api.nvim_win_set_option(vim.fn.bufwinid(bufnr), 'foldexpr', '')
        end,
        is_supported = function(lang)
            for _, v in pairs(treesitter_langs) do
                if v == lang then return true end
            end
            return false
        end,
        enable = true,
    }
}
require 'nvim-treesitter.configs'.setup {
    ensure_installed = treesitter_langs,
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    },
    folding_extra = {
        enable = true,
    },
    playground = {
        enable = true,
        disable = {},
        updatetime = 25,
        persist_queries = false,
        keybindings = {
            show_help = '?',
        },
    },
}

--------------------------
-- Autocompletion setup --
--------------------------

local lspkind = require 'lspkind'
lspkind.init({
    mode = 'symbol_text', -- options: text, text_symbol, symbol_text, symbol
    preset = 'codicons', -- 'default': nerd-fonts, 'codicons' possible
})

local cmp = require 'cmp'

-- see also https://github.com/hrsh7th/nvim-cmp/wiki/Example-mappings

cmp.setup({
    snippet = {
        expand = function(args)
            require('snippy').expand_snippet(args.body)
        end
    },
    window = {
        completion = {
            side_padding = 0,
        },
    },
    mapping = {
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
        ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            else
                fallback()
            end
        end, { "i", "s" }),
        ['<S-Tab>'] = cmp.mapping(function()
            if cmp.visible() then
                cmp.select_prev_item()
            end
        end, { "i", "s" }),
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'nvim_lsp_signature_help' },
    }, {
        { name = 'buffer' },
        { name = 'path' },
    }),
    formatting = {
        fields = { "kind", "abbr", "menu" },
        format = function(entry, vim_item)
            local item = lspkind.cmp_format({
                mode = 'symbol_text',
                maxwidth = 50,
                ellipsis_char = ' .. ',
            })(entry, vim_item)
            local strings = vim.split(item.kind, "%s", {trimempty = true})

            item.kind = " " .. strings[1] .. " "
            item.menu = "   (" .. strings[2] .. ")"

            return item
        end,
    }
})

local capabilities_cmp = require('cmp_nvim_lsp').default_capabilities()

-----------------------------------
-- NVim LSP config configuration --
-----------------------------------

local lspconfig = require('lspconfig')

local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end

    local opts = { noremap = true, silent = true }

    buf_set_keymap('n', '<leader>gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', '<leader>gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', '<leader>gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)

    buf_set_keymap('n', '<leader>K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)

    buf_set_keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '<leader>gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)

    buf_set_keymap('n', '<leader>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)

    buf_set_keymap('n', '<leader>=', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

    -- the root dir can be found in client.config.root_dir?
end

-- better diagnostics
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    function(err, result, ctx, cfg)
        vim.lsp.diagnostic.on_publish_diagnostics(err, result, ctx, cfg)
        vim.diagnostic.setloclist({
            open = false,
        })
    end,
    {
        underline = false,
        signs = true,
        virtual_text = false,
        update_in_insert = false,
    }
)

local included_servers = {
    'ccls', -- ccls can be installed with pacman
    --'rls', -- install using rustup -> rls not available anymore
    'rust_analyzer',
    'bashls', -- install bash-language-server with pacman
    'html', -- install vscode-html-languageserver with pacman
    'cssls', -- install vscode-css-languageserver with pacman -> also for scss!
    'hls', -- haskell-language-server can be installed with pacman
    'pyright', -- install pyright with pacman
    'texlab', -- install texlab with pacman
    'vimls', -- install with npm
    'tsserver', -- install with npm
}
for _, lsp in ipairs(included_servers) do
    if lspconfig[lsp] then
        local lsp_cfg = {
            on_attach = on_attach,
            flags = {
                debounce_text_changes = 50,
            },
            capabilities = capabilities_cmp,
        }
        if (lsp == 'hls') then
            -- added configuration for Haskell
            --  make it so that the root is the working dir as fallback
            --  based on build-in pyright lspconfig
            lsp_cfg.root_dir = function(file_name)
                -- for root_files: see default hls config
                local root_files = {
                    '*.cabal',
                    'stack.yaml',
                    'cabal.project',
                    'package.yaml',
                    'hie.yaml',
                }
                return (lspconfig.util.root_pattern(unpack(root_files))(file_name)
                    or lspconfig.util.find_git_ancestor(file_name)
                    or lspconfig.util.path.dirname(file_name))
            end
        elseif (lsp == 'ccls') then
            -- added configuration for C
            lsp_cfg.init_options = {
                compilationDatabaseDirectory = "build";
                index = {
                    onChange = true;
                };
                cache = {
                    directory = ".lsp-cache";
                };
            }
        elseif (lsp == "rls") then
            -- error on rls -> not available for rust 1.65 -> use rust-analyzer
            lsp_cfg.cmd = {"rustup", "run", "nightly", "rls"}
            lsp_cfg.settings = {
                rust = {
                    unstable_features = true,
                    build_on_save = false,
                    all_features = true,
                }
            }
        elseif (lsp == "rust_analyzer") then
            lsp_cfg.cmd = {"rustup", "run", "nightly", "rust-analyzer"}
            lsp_cfg.settings = {
                ['rust-analyzer'] = {
                }
            }
        elseif (lsp == "html") then
            lsp_cfg.filetypes = {
                'html',
                --'twig.html', -> worse colors and not much pros
            }
        end
        lspconfig[lsp].setup(lsp_cfg)
    else
        error("server, " .. lsp .. " not found!")
    end
end

-- the annoying ones
--  lua language server
local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

lspconfig.sumneko_lua.setup {
    on_attach = on_attach,
    flags = {
        debounce_text_changes = 150,
    },
    cmd = { 'lua-language-server' },
    settings = {
        Lua = {
            runtime = {
                version = 'LuaJIT',
                path = runtime_path,
            },
            diagnostics = {
                globals = { 'vim' },
            },
            workspace = {
                library = vim.api.nvim_get_runtime_file("", true),
            },
            telemetry = {
                enable = false,
            },
        },
    },
}
