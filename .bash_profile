#
# ~/.bash_profile
#

export PATH=$PATH:$HOME/bin:$HOME/usr/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/usr/lib
export BROWSER="firefox"
export EDITOR="nvim"
export VISUAL="nvim"
export QT_QPA_PLATFORMTHEME="gtk2"
export MKL_DEBUG_CPU_TYPE=5
export _JAVA_AWT_WM_NONREPARENTING=1
export MAKEFLAGS=-j$(( $(nproc) / 2))

[[ -f ~/.bashrc ]] && . ~/.bashrc
