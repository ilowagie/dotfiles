#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias cmake2='cmake -GNinja -D CMAKE_C_COMPILER=clang -D CMAKE_CXX_COMPILER=clang++'
alias cp='cp -i'
alias nv='nvim'
alias usb='udisksctl mount -b '
alias fcd='. fcd'
alias grep='grep --color=auto'
alias please='sudo !!'
alias dotconfig='git --git-dir=$HOME/dotfiles --work-tree=$HOME'
alias treesrc='tree -I build'
alias whywontmyrusttestwork='env RUST_BACKTRACE=full cargo test -- --nocapture'
alias yay='yay --editmenu'

if [ `tput colors` == "256" ]; then
  (cat ~/.cache/wal/sequences)
  PS1='[\[\e[1;49;35m\]\u\[\e[m\]@\[\e[1;49;34m\]\h\[\e[m\] \[\e[1;49;37m\]\w\[\e[m\]]\$ '
else
  PS1='[\[\e[1;49;34m\]\u\[\e[m\]@\[\e[1;49;36m\]\h\[\e[m\] \[\e[1;49;95m\]\w\[\e[m\]]\$ '
fi
