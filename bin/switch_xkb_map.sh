#!/bin/sh

layouts=("us" "gr" "ru")

max_index=$((${#layouts[@]} - 1))
current=$(setxkbmap -print -verbose 10 | grep layout | cut -d ' ' -f 6)

# find index of current layout
index=-1
for i in "${!layouts[@]}"; do
    if [[ "${layouts[$i]}" = "${current}" ]]; then
        index=${i}
        break
    fi
done

# get index of next layout
#   check if index was actually found
if [ ${index} -lt 0 ]; then
    echo "ERROR did not find current layout in list of possible layouts" 1>&2
    exit -1
fi
#   wrap-around
[ ${index} -eq ${max_index} ] && next_index=0 || next_index=$((${index} + 1))

# set layout to next_layout
setxkbmap -layout ${layouts[${next_index}]}
