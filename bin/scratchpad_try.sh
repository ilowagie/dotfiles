#!/bin/bash

[ -z $(cat /tmp/scratchpad) ] && (bspc query -N -n .floating | tee >(head -1 > /tmp/scratchpad) >(tail -1 > /tmp/scratchpad_calc))

id=$(cat /tmp/scratchpad)
[ -n $id ] && (bspc node $id --flag hidden -f)
