#!/bin/bash


id=$(cat /tmp/last_biggest_node)

[ -z $(bspc query -N -n $id) ] && bspc node biggest.local -s smallest.local || bspc node biggest.local -s $id
